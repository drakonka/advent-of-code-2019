package util

import (
	"fmt"
	"os"
	"path"
)

func GetInputFile(filename string) *os.File {
	filepath := path.Join(path.Dir(filename), "input.txt")

	var err error
	inputFile, err := os.Open(filepath)
	if err != nil {
		fmt.Printf("Error: %s", err)
		return nil
	}
	return inputFile
}
