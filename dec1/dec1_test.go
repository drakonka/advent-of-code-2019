package main

import (
	"strings"
	"testing"
)

var testCases = []struct {
	mass        string
	wantPartOne int
	wantPartTwo int
}{
	{"12", 2, 2},
	{"14", 2, 2},
	{"1969", 654, 966},
	{"100756", 33583, 50346},
	{"sldjf", -1, -1},
}

func TestPartOne(t *testing.T) {
	for _, tc := range testCases {
		t.Run(tc.mass, func(t *testing.T) {
			res, _ := runPartOne(strings.NewReader(tc.mass))
			if res != tc.wantPartOne {
				t.Errorf("Got %d, expected %d", res, tc.wantPartOne)
			}
		})
	}
}

func TestPartTwo(t *testing.T) {
	for _, tc := range testCases {
		t.Run(tc.mass, func(t *testing.T) {
			res, _ := runPartTwo(strings.NewReader(tc.mass))
			if res != tc.wantPartTwo {
				t.Errorf("Got %d, expected %d", res, tc.wantPartTwo)
			}
		})
	}
}

func BenchmarkPartOne(b *testing.B) {
	for n := 0; n < b.N; n++ {
		runPartOne(nil)
	}
}

func BenchmarkPartTwo(b *testing.B) {
	for n := 0; n < b.N; n++ {
		runPartTwo(nil)
	}
}
